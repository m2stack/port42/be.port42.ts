export default interface Factory<I, O> {
    (params: I): O;
}
