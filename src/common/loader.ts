import { Router } from 'express';
import * as fs from 'fs';
import * as path from 'path';
import { AppConfig } from './types';
import Port42Service from '../services/port42.service';

const moduleLoader = (name: string, service: Port42Service, config: AppConfig) =>
    // tslint:disable-next-line:ter-max-len
    (require(`../modules/${name}/${name}Module`).default as (service: Port42Service, config: AppConfig) => Router)(service, config);

export const graphqlModule = (name: string) => require(`../graphql/schema/${name}/index`).default;

export const dirs = (p: string) =>
    fs.readdirSync(p).filter(f => fs.statSync(path.join(p, f)).isDirectory());

export default moduleLoader;
