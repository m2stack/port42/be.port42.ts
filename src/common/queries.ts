import { Pool } from 'pg';

const pool = new Pool({
    user: 'me',
    host: 'localhost',
    database: 'api',
    password: '123',
    port: 5432,
});
export default pool;
