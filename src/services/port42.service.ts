import SwaggerService from './swagger.service';
import { AppConfig, AsyncStartStop } from '../common/types';

export default class Port42Service implements AsyncStartStop {
    constructor(
        private config: AppConfig,
    ) {}

    readonly swaggerService = new SwaggerService(this.config);

    readonly start: () => Promise<void>;
    readonly stop: () => Promise<void>;
}
