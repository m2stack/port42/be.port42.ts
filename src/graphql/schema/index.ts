
import { ApolloServer } from 'apollo-server-express';
import { merge } from 'lodash';
import { graphqlModule, dirs } from '../../common/loader';
import logger from '../../utils/logger';

// tslint:disable-next-line:variable-name
const Query = `
 type Query {
   status: String
 }
`;

// tslint:disable-next-line:variable-name
const Mutation = `
 type Mutation {
   _empty: String
 }
`;

let resolvers = {
    Query: {
        status: () => 'ok',
    },
};

const typeDefs = [Query, Mutation];

const graphqlDirs = dirs('./src/graphql/schema');
graphqlDirs.forEach((folderName) => {
    logger.debug(`registering graphql folder '${folderName}'`);
    const qraphqlModule = graphqlModule(folderName);
    resolvers = merge(resolvers, qraphqlModule.resolvers);
    typeDefs.push(qraphqlModule.types);
});


const schema = new ApolloServer({
    typeDefs,
    resolvers,
    playground: {
        settings: {
            'editor.theme': 'light',
        },
    },
});

export default schema;
