// tslint:disable-next-line:variable-name
const TestDb = `
  type TestDb {
    name: String!
    email: String!
  }
`;

export const types = (): string[] => [TestDb];

export const typeResolvers = {

};
