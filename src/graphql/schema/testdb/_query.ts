import { IQueryResolver } from '../../../common/graphqlTypes';
import pool from '../../../common/queries';

// tslint:disable-next-line:variable-name
const Query = `
  extend type Query {
    testdb: [TestDb]
  }
`;

export const queryTypes = (): string[] => [Query];

export const queryResolvers: IQueryResolver = {
    Query: {
        testdb: () => pool.query('SELECT * FROM users ORDER BY id ASC')
        .then(result => result.rows),
    },
};
