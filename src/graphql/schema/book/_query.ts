import { IQueryResolver } from '../../../common/graphqlTypes';

// tslint:disable-next-line:variable-name
const Query = `
  extend type Query {
    books: [Book]
  }
`;

export const queryTypes = (): string[] => [Query];

export const queryResolvers: IQueryResolver = {
    Query: {
        books: () => ([
            {
                title: "Harry Potter and the Sorcerer's stone1",
                author: 'J.K. Rowling',
            },
            {
                title: 'Jurassic Park1',
                author: 'Michael Crichton',
            },
        ]),
    },
};
