import { ModuleDef } from '../../common/types';
import { Router } from 'express';
import * as bodyParser from 'body-parser';
import TestController from './controllers/test.controller';
import Context from './common/test.context';
import { setupModule } from '../../common/registrar';
import { errorHandler } from '../../common/errors';

const allAuthenticated = () => true;

export default (): Router => {
    const testController = TestController();

    const pathMapping: ModuleDef<Context> = {
        '/info': {
            get: {
                action: testController.info,
                authorization: allAuthenticated,
            },
        },
        '/dbtest': {
            get: {
                action: testController.dbtest,
                authorization: allAuthenticated,
            },
        },
    };

    const router = Router();
    router.use(bodyParser.json());
    setupModule(pathMapping, router, Context.create);
    // register error handler(s)
    router.use(errorHandler);

    return router;
};
