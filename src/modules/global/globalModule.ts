import { Router } from 'express';
import * as cors from 'cors';

const VERSION = require('../../../package.json').version;

export default (): Router => {
    const router = Router();

    router.use(cors());

    // tslint:disable-next-line:variable-name
    router.get('/status/json', (_req, res) => {
        res.send({
            version: `${VERSION}-server`,
            allSystemsWorking: true,
        });
    });

    // tslint:disable-next-line:variable-name
    router.post('/webhook/test', (_req, res) => {
        res.send({
            status: 'ok',
        });
    });

    return router;
};
