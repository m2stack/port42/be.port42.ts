import Logger from './utils/logger';
import config from './config/server.config';
import { AppConfig } from './common/types';
import serverFactoryFactory, { WebServer } from './common/port42.server';
import Port42Service from './services/port42.service';

Logger.info('Environment setup:');
Logger.info(`ENV                ${config.env}`);
Logger.info(`PORT               ${config.port}`);
Logger.info(`SERVER_NAME        ${config.serverName}`);
Logger.info(`API_URL_BASE       ${config.apiUrlBase}`);
Logger.info(`CORS_DOMAIN        ${config.corsDomain}`);
Logger.info(`DONT_USE_GRAPHQL   ${config.dontUseGraphql}`);
Logger.info(`GRAPHQL_PATH       ${config.graphqlPath}`);
Logger.info(`HTTP_SCHEMA        ${config.httpSchema}`);

const APP_CONFIG: AppConfig = {
    server: config
}

const serverFactory = serverFactoryFactory({
    modules: {
        '': 'global',
        '/api/v1/test': 'test'
    },
    config: APP_CONFIG,
    port: APP_CONFIG.server.port
})
const startServer = (server: WebServer): Promise<WebServer> => server.start().then(() => server);

const buildStructure = (cfg: AppConfig): WebServer => serverFactory(new Port42Service(cfg));

const waitForShutdown = (server: WebServer): void => {
	const stopServer = (): void =>{
		server
		.stop()
		.then(()=>{
			process.exit()
		});
    };

	// listen for TERM signal .e.g. kill
	process.on('SIGTERM', stopServer);

	// listen for INT signal e.g. Ctrl-C
	process.on('SIGINT', stopServer)
};

Promise
    .resolve(APP_CONFIG)
    .then(buildStructure)
    .then(startServer)
    .then(waitForShutdown)
    .catch(e => {
        Logger.error("Unhandled exception during server initialization:", e);
        process.exit(-1)
    });