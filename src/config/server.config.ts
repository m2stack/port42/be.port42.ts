import * as dotenv from 'dotenv';
import constants from '../common/constants';
import { ServerConfig } from '../common/types';

dotenv.config();

const commonConfig: ServerConfig = {
    env: process.env.NODE_ENV || 'development',
    apiUrlBase: process.env.API_URL_BASE || constants.apiDefaultBasePath,
    port: parseInt(process.env.PORT, 10) || 4998,
    corsDomain: process.env.CORS_DOMAIN || '*',
    serverName: process.env.SERVER_NAME || 'localhost',
    dontUseGraphql: !!+process.env.DONT_USE_GRAPHQL || false,
    graphqlPath: process.env.GRAPHQL_PATH || constants.graphqlDefaultPath,
    httpSchema: process.env.HTTP_SCHEMA || constants.httpDefaultSchema,
};

export default commonConfig;
