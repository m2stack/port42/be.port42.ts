CREATE ROLE me WITH LOGIN PASSWORD '123';
ALTER ROLE me CREATEDB;
CREATE DATABASE api;
\c api
CREATE TABLE users (
  ID SERIAL PRIMARY KEY,
  name VARCHAR(30),
  email VARCHAR(30)
);

INSERT INTO users (name, email)
  VALUES ('Martin1', 'martin1@example.com'), ('George', 'george@example.com');
GRANT ALL PRIVILEGES ON DATABASE api TO me;
GRANT ALL PRIVILEGES ON TABLE users TO me;